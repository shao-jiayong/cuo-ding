﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.Common
{
    /// <summary>
    /// 基础参数类
    /// </summary>
    public class BaseParmEntity
    {
        /// <summary>
        /// 页码(第几页)
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 页数(每页行数)
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 排序方式
        /// </summary>
        public string Order { get; set; } = "CreateTime desc";

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 关键词
        /// </summary>
        public string KeyWord { get; set; }

    }

}
