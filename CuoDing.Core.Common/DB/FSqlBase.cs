﻿using FreeSql;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.Common
{
    public sealed class FBase
    {

        private static string connStr = Appsettings.app(new string[] { "connectionString" }).ObjToString();
        private static int dbType = Appsettings.app(new string[] { "dbType" }).ObjToInt();
        //private static string ConnStr = ConfigurationManager.ConnectionStrings["FreeSql:Default"].ConnectionString;
        
        public static readonly IFreeSql _fsql = new FreeSqlBuilder()
                                            .UseConnectionString((DataType)dbType, connStr)
                                            //.UseSyncStructureToUpper(true)
                                            //.UseNameConvert(FreeSql.Internal.NameConvertType.ToUpper)
                                            .Build();


        //public FBase()
        //{
        //    _fsql.Aop.CurdBefore += (s, e) =>
        //    {
        //        //System.Console.WriteLine(e.Sql);
        //        log.Debug(string.Format("\r\n---{0}---\r\n{1}", System.DateTime.Now,e.Sql));
        //    };
        //}
    }

    public abstract class FSqlBase
    {
        public IFreeSql fsql
        {
            get { return FBase._fsql; }
        }
        public FSqlBase()
        {
            fsql.Aop.CurdBefore += (s, e) =>
            {
                System.Console.WriteLine(e.Sql);
            };
        }
    }
}
