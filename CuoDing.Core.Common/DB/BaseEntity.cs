﻿using FreeSql.DataAnnotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.Common
{
	/// <summary>
	/// 基础实体类
	/// </summary>
    public class BaseEntity
    {

		/// <summary>
		/// 主键Id
		/// </summary>
		[JsonProperty, Column(IsIdentity = true, IsPrimary = true)]
		public long? Id { get; set; }


		/// <summary>
		/// 删除标志
		/// </summary>
		[JsonProperty]
		public bool? DeleteMark { get; set; }


		/// <summary>
		/// 创建时间
		/// </summary>
		[JsonProperty]
		public DateTime? CreateTime { get; set; }

		/// <summary>
		/// 修改时间
		/// </summary>
		[JsonProperty]
		public DateTime? ModifyTime { get; set; }
	}
}
