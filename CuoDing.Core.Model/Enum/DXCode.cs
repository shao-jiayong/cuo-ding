﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.Model.Enum
{
    /// <summary>
    /// 状态枚举类
    /// </summary>
    public enum DXCode : short
    {
        Unknow = -1,
        /// <summary>
        /// 成功
        /// </summary>
        Success = 0,
        /// <summary>
        /// 失败
        /// </summary>
        Failure = -128,
        /// <summary>
        /// 错误请求
        /// </summary>
        BadRequest = -256,
        ///// <summary>
        ///// 执行失败
        ///// </summary>
        //ActionExecutingFailure = -10002,
        ///// <summary>
        ///// 意外失败
        ///// </summary>
        //UnexpectedFailure = -10003,
        /// <summary>
        /// 非法请求（登陆超时）
        /// </summary>
        Unauthorized = -16,
    }
}
