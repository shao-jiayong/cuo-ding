﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.Model.ViewModels
{
    /// <summary>
    /// 基础查询类
    /// </summary>
    public class BaseParam
    {
        /// <summary>
        /// 页码(第几页)
        /// </summary>
        public int? PageIndex { get; set; }
        /// <summary>
        /// 页数(每页行数)
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
    }
}
