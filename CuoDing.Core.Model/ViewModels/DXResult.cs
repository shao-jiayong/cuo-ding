﻿using CuoDing.Core.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.Model.ViewModels
{
    /// <summary>
    /// 消息返回类
    /// </summary>
    public class DXResult
    {
        /// <summary>
        /// 状态  
        /// </summary>
        public DXCode code { get; set; } = DXCode.Unknow;
        /// <summary>
        /// 描述
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// 具体数据 
        /// </summary>
        public dynamic data { get; set; }
        /// <summary>
        /// 数据影响行数
        /// </summary>
        public long? count { get; set; }


    }
}
