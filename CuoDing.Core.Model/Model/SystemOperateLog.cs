﻿using FreeSql.DataAnnotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.Model.Model
{
	/// <summary>
	///  操作日志
	/// </summary>
	[JsonObject(MemberSerialization.OptIn), Table(Name = "sys_operate_log")]
	public class SystemOperateLog
	{

		/// <summary>
		/// 编号
		/// </summary>
		[JsonProperty, Column(IsIdentity = true, IsPrimary = true)]
		public long? Id { get; set; }
		/// <summary>
		/// ip地址
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string IP { get; set; }


		/// <summary>
		/// 接口名称
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string ApiLabel { get; set; }

		/// <summary>
		/// 提交方式
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string ApiMethod { get; set; }

		/// <summary>
		/// 接口地址
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string ApiPath { get; set; }


		/// <summary>
		/// 状态
		/// </summary>
		[JsonProperty]
		public int? LogStatus { get; set; }


		/// <summary>
		/// 操作参数
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(MAX)")]
		public string Params { get; set; }

		/// <summary>
		/// 结果
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(MAX)")]
		public string Result { get; set; }


		/// <summary>
		/// 昵称
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string NickName { get; set; }


		/// <summary>
		/// 浏览器
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string Browser { get; set; }

		/// <summary>
		/// 操作系统
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string Os { get; set; }

		/// <summary>
		/// 设备
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string Device { get; set; }

		/// <summary>
		/// 浏览器信息
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(200)")]
		public string BrowserInfo { get; set; }

		/// <summary>
		/// 耗时（毫秒）
		/// </summary>
		public long ElapsedMilliseconds { get; set; }

		/// <summary>
		/// 删除标志
		/// </summary>
		[JsonProperty]
		public bool? DeleteMark { get; set; }



		/// <summary>
		/// 创建时间
		/// </summary>
		[JsonProperty]
		public DateTime? CreateTime { get; set; }

		/// <summary>
		/// 编辑时间
		/// </summary>
		[JsonProperty]
		public DateTime? ModifyTime { get; set; }
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string CreateUser { get; set; }
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string ModifyUser { get; set; }


	}
}
