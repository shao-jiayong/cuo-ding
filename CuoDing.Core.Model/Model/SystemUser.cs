﻿using FreeSql.DataAnnotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.Model.Model
{
	/// <summary>
	///  用户表
	/// </summary>
	[JsonObject(MemberSerialization.OptIn), Table(Name = "sys_user")]
	public class SystemUser
	{

		/// <summary>
		/// 编号
		/// </summary>
		[JsonProperty, Column(IsIdentity = true, IsPrimary = true)]
		public long? Id { get; set; }

		/// <summary>
		/// 姓名
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string Name { get; set; }

		/// <summary>
		/// 账号
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string UserCode { get; set; }

		/// <summary>
		/// 密码
		/// </summary>
		[JsonProperty, Column(DbType = "varchar(50)")]
		public string Password { get; set; }

		/// <summary>
		/// 删除标志
		/// </summary>
		[JsonProperty]
		public bool? DeleteMark { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		[JsonProperty]
		public DateTime? CreateTime { get; set; }

		/// <summary>
		/// 修改时间
		/// </summary>
		[JsonProperty]
		public DateTime? ModifyTime { get; set; }



	}
}
