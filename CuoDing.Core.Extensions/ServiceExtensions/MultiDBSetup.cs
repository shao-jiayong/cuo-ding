﻿using CuoDing.Core.Common;
using FreeSql;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CuoDing.Core.Extensions
{
    /// <summary>
    /// 多数据库扩展
    /// </summary>
    public static class MultiDBSetup
    {
        public static void AddMultiDB(this IServiceCollection services)
        {
            var fsql = new MultiFreeSql();
            List<MultiDb> dbsList = Appsettings.app<MultiDb>("Dbs").ToList();
            if (dbsList.Any())
            {
                foreach (var dbObj in dbsList)
                {
                    fsql.Register(dbObj.Name, () => new FreeSqlBuilder().UseConnectionString(dbObj.DbType, dbObj.ConnectionString).Build());

                }
            }
            services.AddSingleton<IFreeSql>(fsql);

        }
    }
}
