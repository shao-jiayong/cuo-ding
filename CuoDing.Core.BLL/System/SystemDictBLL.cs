﻿using CuoDing.Core.DAL;
using CuoDing.Core.Model;
using CuoDing.Core.Model.Enum;
using CuoDing.Core.Model.Model;
using CuoDing.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.BLL
{
    public class SystemDictBLL : ISystemDictBLL
    {
        private readonly SystemDictDAL _dal;

        /// <summary>
        /// 构造函数注入
        /// </summary>
        /// <param name="dal"></param>
        public SystemDictBLL(SystemDictDAL dal)
        {
            _dal = dal;
        }

        /// <summary>
        /// 新增或更新字典
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<DXResult> UpdateOrInsert(SystemDict model)
        {
            DXResult dXResult = new DXResult();

            int i = await _dal.UpdateOrInsert(model);
            if (i > 0)
            {
                dXResult.code = DXCode.Success;
                dXResult.msg = "新增成功";
            }
            else
                dXResult.code = DXCode.Failure;
            return dXResult;
        }
        /// <summary>
        /// 获取字典
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SystemDict> Get(long id)
        {
            SystemDict dXResult = await _dal.GetById(id);
            return dXResult;
        }
        /// <summary>
        /// 获取字典列表(分页)
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<DXResult> DictList(BaseParam param)
        {
            DXResult dXResult = new DXResult();
            dXResult.code = DXCode.Success;
            dXResult.msg = "获取成功";
            int? pageIndex = 1, pageSize = 10;
            try
            {
                #region 查询条件
                Expression<Func<SystemDict, bool>> exp = it => it.DeleteMark == false;

                if (param.PageIndex.HasValue)
                {
                    pageIndex = param.PageIndex;
                }
                if (param.PageSize.HasValue)
                {
                    pageSize = param.PageSize;
                }
                if (!string.IsNullOrEmpty(param.Name))
                {
                    exp = exp.And(it => it.Name.Contains(param.Name));
                }
                if (!string.IsNullOrEmpty(param.Code))
                {
                    exp = exp.And(it => it.Code.Contains(param.Code));
                }
                #endregion

                long count = 0;
                dXResult.data = await _dal.GetList(pageSize.Value, pageIndex.Value, ref count, exp);
                dXResult.count = count;
            }
            catch (Exception ex)
            {
                dXResult.code = DXCode.Failure;
                dXResult.msg = ex.Message;
            }
            return dXResult;
        }

    }
}
