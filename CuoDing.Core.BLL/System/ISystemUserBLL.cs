﻿using CuoDing.Core.Model.Model;
using CuoDing.Core.Model.ViewModels;
using System.Threading.Tasks;

namespace CuoDing.Core.BLL
{
    public partial interface ISystemUserBLL
    {
        Task<DXResult> AddUsers(SystemUser model);
        Task<SystemUser> GetUser(long id);
        DXResult Update(SystemUser param);
        Task<DXResult> LoginToken(SystemUser param);
        Task<DXResult> TokenGetById(long id);
    }
}