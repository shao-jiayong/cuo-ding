﻿using CuoDing.Core.Model.Model;
using CuoDing.Core.Model.ViewModels;
using System.Threading.Tasks;

namespace CuoDing.Core.BLL
{
    public partial interface ISystemOperateLogBLL
    {
        Task<DXResult> AddLog(SystemOperateLog model);
        Task<SystemOperateLog> GetUser(long id);
        DXResult Update(SystemOperateLog item);
    }
}