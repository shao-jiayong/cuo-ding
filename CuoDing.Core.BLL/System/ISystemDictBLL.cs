﻿using CuoDing.Core.Model.Model;
using CuoDing.Core.Model.ViewModels;
using System.Threading.Tasks;

namespace CuoDing.Core.BLL
{
    public partial interface ISystemDictBLL
    {
        Task<DXResult> UpdateOrInsert(SystemDict model);
        Task<SystemDict> Get(long id);
        Task<DXResult> DictList(BaseParam param);
    }
}