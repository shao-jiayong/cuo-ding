﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CuoDing.Core
{
    /// <summary>
    /// 不记录操作日志的属性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class NoLogAttribute : Attribute
    {
    }
}
