﻿using Microsoft.AspNetCore.ResponseCompression;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace CuoDing.Core.WebApi
{
    /// <summary>
    /// 自定义压缩
    /// </summary>
    public class CustomCompressionProvider : ICompressionProvider
    {
        public string EncodingName => "br";
        public bool SupportsFlush => true;
        public Stream CreateStream(Stream outputStream)
        {
            return new BrotliStream(outputStream,
             CompressionLevel.Fastest, false);
        }

    }
}
