﻿using CuoDing.Core.BLL;
using CuoDing.Core.Model.Model;
using CuoDing.Core.Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CuoDing.Core.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class DictController : ControllerBase
    {
        private readonly ISystemDictBLL _systemDictBLL;

        public DictController(ISystemDictBLL systemDictBLL)
        {
            _systemDictBLL = systemDictBLL;
        }
        /// <summary>
        /// 新增或修改字典
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<dynamic> AddorUpdateDict(SystemDict param)
        {
            var dXResult = await _systemDictBLL.UpdateOrInsert(param);
            return dXResult;
        }

        /// <summary>
        /// 字典列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<dynamic> DictList(BaseParam param)
        {
            var dXResult = await _systemDictBLL.DictList(param);
            return dXResult;
        }
    }
}
