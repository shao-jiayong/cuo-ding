﻿using CuoDing.Core.BLL;
using CuoDing.Core.Model.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CuoDing.Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly ISystemUserBLL _systemUserBLL;
        /// <summary>
        /// 构造函数注入
        /// </summary>
        public UserController(ISystemUserBLL systemUserBLL, ILogger<UserController> logger)
        {
            _systemUserBLL = systemUserBLL;
            _logger = logger;
        }
        [HttpPost("addUser")]
        public async Task<dynamic> Add(SystemUser user)
        {
            var obj = await _systemUserBLL.AddUsers(user);
            return obj;
        }

        [HttpGet("getUser")]
        public async Task<dynamic> Get(long id)
        {
            //_logger.LogError("LogError3333");
            var obj = await _systemUserBLL.GetUser(id);
            return obj;
        }
        [NoLog]
        [HttpPost("updateUser")]
        public dynamic UpdateUser(SystemUser user)
        {
            var obj = _systemUserBLL.Update(user);
            return obj;
        }
    }
}
