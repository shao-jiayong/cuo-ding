﻿using Microsoft.AspNetCore.Builder;

namespace CuoDing.Core.WebApi
{
    /// <summary>
    /// 中间件扩展帮助类
    /// </summary>
    public static class MiddlewareExtensions
    {
        /// <summary>
        /// 请求上下文中间件
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseHttpContextMildd(this IApplicationBuilder app)
        {
            return app.UseMiddleware<HttpContextMiddleware>();
        }
    }
}
