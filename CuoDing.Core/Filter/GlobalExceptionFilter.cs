﻿using CuoDing.Core.Model.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace CuoDing.Core.Filter
{
    /// <summary>
    /// 全局异常错误日志
    /// </summary>
    public class GlobalExceptionsFilter : IExceptionFilter
    {
        private readonly IWebHostEnvironment _env;
        private readonly ILogger<GlobalExceptionsFilter> _logger;

        public GlobalExceptionsFilter(IWebHostEnvironment env, ILogger<GlobalExceptionsFilter> logger)
        {
            _env = env;
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            var json = new DXResult();
            json.code = Model.Enum.DXCode.BadRequest;
            json.msg = context.Exception.Message;//错误信息
            var errorAudit = "Unable to resolve service for";//特殊错误信息
            if (!string.IsNullOrEmpty(json.msg) && json.msg.Contains(errorAudit))
            {
                json.msg = json.msg.Replace(errorAudit, $"（若新添加服务，需要重新编译项目）{errorAudit}");
            }
            if (_env.IsDevelopment())
            {
                json.msg = context.Exception.StackTrace;//堆栈信息
            }
            context.Result = new InternalServerErrorObjectResult(json);

            //输出错误日志信息
            //_logger.LogError(json.Message + WriteLog(json.Message, context.Exception));
            _logger.LogError(json.msg + string.Format("\r\n【自定义错误】：{0} \r\n【异常类型】：{1} \r\n【异常信息】：{2} \r\n【堆栈调用】：{3}", new object[] { json.msg,
                context.Exception.GetType().Name, context.Exception.Message, context.Exception.StackTrace }));


        }

    }
    public class InternalServerErrorObjectResult : ObjectResult
    {
        public InternalServerErrorObjectResult(object value) : base(value)
        {
            StatusCode = StatusCodes.Status500InternalServerError;
        }
    }
    ////返回错误信息
    //public class JsonErrorResponse
    //{
    //    /// <summary>
    //    /// 生产环境的消息
    //    /// </summary>
    //    public string Message { get; set; }
    //    /// <summary>
    //    /// 开发环境的消息
    //    /// </summary>
    //    public string DevelopmentMessage { get; set; }
    //}

}
