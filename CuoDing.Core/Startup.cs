using Autofac;
using CuoDing.Core.BLL;
using CuoDing.Core.Common;
using CuoDing.Core.DAL;
using CuoDing.Core.Extensions;
using CuoDing.Core.Filter;
using CuoDing.Core.Logs;
using CuoDing.Core.Model.Enum;
using CuoDing.Core.Model.ViewModels;
using CuoDing.Core.WebApi;
using Elasticsearch.Net;
using FreeSql;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Minio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // 此方法由运行时调用。使用此方法向容器添加服务。
        public void ConfigureServices(IServiceCollection services)
        {

            //三种生命周期：Transient（暂时）Scoped（作用域）Singleton（单例）。

            services.AddSingleton(new Appsettings(Configuration));
            //用户信息
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IUser, User>();
            //services.TryAddSingleton<IEsClientProvider, EsClientProvider>();
            //services.AddSingleton<IProductService, ProductService>();
            //services.AddSingleton(new SystemUserDAL());
            //services.AddElasticsearch(Configuration);

            services.AddScoped<ILogHandler, LogHandler>();
            services.AddResponseCompression(options =>
            {
                options.Providers.Add(new CustomCompressionProvider());
            });
            //var pool = new SingleNodeConnectionPool(new Uri("http://localhost:9200")); //or other server adress
            //var settings = new ConnectionSettings(pool)
            //    .DefaultIndex("exam"); //add Index name
            //var client = new ElasticClient(settings);
            //services.AddSingleton(client);
            #region minio
            var minioClient = new MinioClient("127.0.0.1:9090","minioadmin","12345678");
            services.AddSingleton(minioClient);
            #endregion
            #region 身份认证授权
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    SaveSigninToken = true,//保存token,后台验证token是否生效(重要)
                    ValidateIssuer = true,//是否验证Issuer
                    ValidateAudience = true,//是否验证Audience
                    ValidateLifetime = true,//是否验证失效时间
                    ValidateIssuerSigningKey = true,//是否验证SecurityKey
                    ValidAudience = Appsettings.app(new string[] { "Jwt", "Audience" }).ToString(),//订阅者
                    ValidIssuer = Appsettings.app(new string[] { "Jwt", "Issuer" }).ToString(),//Issuer，这两项和前面签发jwt的设置一致
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Appsettings.app(new string[] { "Jwt", "SecurityKey" }).ToString()))//密钥
                };
                options.Events = new JwtBearerEvents()
                {
                    OnChallenge = context =>
                    {
                        context.HandleResponse();
                        context.Response.Clear();
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 401;
                        //context.Response.WriteAsync(new { message = "授权未通过", status = false, code = 401 }.ToJson());
                        context.Response.WriteAsync(new DXResult { code = DXCode.Unauthorized, msg = "授权未通过" }.ToJson());
                        return Task.CompletedTask;
                    }
                };
            });
            #endregion
            #region 多数据配置
            if (Configuration["DbsOpen"].ObjToBool())
            {
                services.AddMultiDB();
            }
            #endregion
            services.AddControllers(options =>
            {
                //添加全局异常过滤器
                options.Filters.Add<GlobalExceptionsFilter>();
                //日志过滤器
                options.Filters.Add<LogActionFilter>();
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CuoDing.Core", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "JWT授权token前面需要加上字段Bearer与一个空格,如Bearer token",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            #region AutoFac IOC容器,实现批量依赖注入的容器
            try
            {
                #region SingleInstance
                //无接口注入单例
                var assemblyDAL = Assembly.Load("CuoDing.Core.DAL");
                builder.RegisterAssemblyTypes(assemblyDAL)
                .SingleInstance();

                #endregion

                #region Service
                var assemblyServices = Assembly.Load("CuoDing.Core.BLL");
                builder.RegisterAssemblyTypes(assemblyServices)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope()
                .PropertiesAutowired();// 属性注入
                #endregion


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "\n" + ex.InnerException);
            }
            #endregion
        }
        /// 此方法由运行时调用。使用此方法配置HTTP请求管道。
        /// </summary>
        /// 中间件的注册顺序严格按照官方配置推荐依次顺序，更多请看https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/middleware/?view=aspnetcore-5.0
        /// ExceptionHandler=>HSTS=>HttpsRedirection=>Static Files=>CORS=>Authentication=>Authorization=>自定义中间组件=》Endpoint
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CuoDing.Core v1"));
            }
            app.UseResponseCompression();
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            if (Appsettings.app(new string[] { "IsEncrypt" }).ObjToBool())
            {
                //请求上下文中间件
                app.UseHttpContextMildd();
            }
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
