﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.DAL
{
    public interface IBaseDAL<TEntity, TKey> where TEntity : class
    {
        Task<TEntity> QueryById(TKey id);
    }
}
