﻿using CuoDing.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.DAL
{
    public class BaseDAL<TEntity, TKey> : IBaseDAL<TEntity,TKey> where TEntity : class, new()
    {
        private readonly FSqlBase _fsql;

        public BaseDAL(FSqlBase fsql)
        {
            _fsql = fsql;
        }
        public async Task<TEntity> QueryById(TKey id)
        {
            //return await Task.Run(() => _db.Queryable<TEntity>().InSingle(objId));
            return await _fsql.fsql.Queryable<TEntity>().WhereDynamic(id).ToOneAsync<TEntity>();
        }
    }
}
