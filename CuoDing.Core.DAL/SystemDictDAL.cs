﻿using CuoDing.Core.Common;
using CuoDing.Core.Model;
using CuoDing.Core.Model.Enum;
using CuoDing.Core.Model.Model;
using CuoDing.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.DAL
{
    public class SystemDictDAL : FSqlBase
    {
        #region CURD(增删查改)
        public Task<List<SystemDict>> GetList(int pageSize, int pageIndex, ref long count, Expression<Func<SystemDict, bool>> where = null, string order = null)
        {
            return fsql.Queryable<SystemDict>()
                .WhereIf(where != null, where).Count(out count).Page(pageIndex, pageSize).OrderBy(!string.IsNullOrWhiteSpace(order), order).ToListAsync();
        }
        public Task<SystemDict> GetById(long id)
        {
            return fsql.Queryable<SystemDict>().Where(it => it.Id == id).FirstAsync();
        }

        public Task<int> UpdateOrInsert(SystemDict item)
        {
            if (item?.Id > 0)
            {
                return fsql.Update<SystemDict>().SetSource(item)
                        .IgnoreColumns(it => new { it.CreateTime })
                        .ExecuteAffrowsAsync();
            }
            else
            {
                item.CreateTime = DateTime.Now;
                item.DeleteMark = false;
                return fsql.Insert(item).ExecuteAffrowsAsync();
            }
        }

        public Task<int> Delete(long id)
        {
            return fsql.Delete<SystemDict>().Where(it => it.Id == id).ExecuteAffrowsAsync();
        }
        #endregion
    }
}
