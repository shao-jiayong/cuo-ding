﻿using CuoDing.Core.Common;
using CuoDing.Core.Model;
using CuoDing.Core.Model.Enum;
using CuoDing.Core.Model.Model;
using CuoDing.Core.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CuoDing.Core.DAL
{
    public class SystemOperateLogDAL : FSqlBase
    {
        #region CURD(增删查改)
        public Task<List<SystemOperateLog>> GetList(int pageSize, int pageIndex, ref long count, Expression<Func<SystemOperateLog, bool>> where = null, string order = null)
        {
            return fsql.Queryable<SystemOperateLog>()
                .WhereIf(where != null, where).Count(out count).Page(pageIndex, pageSize).OrderBy(!string.IsNullOrWhiteSpace(order), order).ToListAsync();
        }
        public Task<SystemOperateLog> GetById(long id)
        {
            return fsql.Queryable<SystemOperateLog>().Where(it => it.Id == id).FirstAsync();
        }

        public Task<int> UpdateOrInsert(SystemOperateLog item)
        {
            if (item?.Id > 0)
            {
                return fsql.Update<SystemOperateLog>().SetSource(item)
                        .IgnoreColumns(it => new { it.CreateTime })
                        .ExecuteAffrowsAsync();
            }
            else
            {
                item.CreateTime = DateTime.Now;
                item.DeleteMark = false;
                return fsql.Insert(item).ExecuteAffrowsAsync();
            }

        }

        public Task<int> Delete(long id)
        {
            return fsql.Delete<SystemOperateLog>().Where(it => it.Id == id).ExecuteAffrowsAsync();
        }
        #endregion
    }
}
