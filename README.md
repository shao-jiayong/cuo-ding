# CuoDing

#### 名称由来
闽南人常说："厝顶上有出戏。"这戏，"剧情"丰富，有奔马逐凤、锦鸡啼春、麒麟吐玉书、双鹿嬉戏、九鲤献颂、鸳鸯荷香、龙腾虎跃、双虎对视等题材。

而唱好这出"戏"，靠的是一双手、一把剪钳、一堆瓷片、一些水泥土。它，就是"堆剪"技艺。

传统的工艺我可能不懂，但是我想用现代的技术完成这门精美的艺术，是的，代码也是一门艺术，每一个项目都是自己的一个工艺品。

#### 软件架构
.Net5+FreeSql+三层架构

详细实现文档，可以参考我的博客https://blog.csdn.net/shaojiayong

#### 环境要求

1.  Visual Studio 2019 16.8 +
2.  .NET 5 SDK +
3.  .Net Standard 2.1 +

#### 功能模块(持续开发中)

1.  接入国产数据库ORM组件 —— FreeSql，封装数据库操作；
2.  使用 Nlog 日志框架，做日志记录；
3.  使用 AutoFac 做依赖注入容器，并提供批量服务注入
4.  使用 Swagger 做api文档；
5.  封装 JWT 自定义策略授权
...

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
